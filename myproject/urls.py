from django.conf.urls import include, url
from django.contrib import admin
from ajax_select import urls as ajax_select_urls

urlpatterns = [
    url(r'^auth/', include('authentication.urls', namespace="auth")),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('main.urls')),
    url(r'^messages/',
        include('postman.urls', namespace='postman', app_name='postman')),
    url(r'^intern/messages/lookups/',
        include(ajax_select_urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^comments/', include('comments.urls')),
]
