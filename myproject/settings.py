"""
Django settings for myproject project.

Generated by 'django-admin startproject' using Django 1.9.2.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""

import os
import socket
DEPLOYED = socket.gethostname().endswith('webfaction.com')

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'y67fkbuk9lr_bah-i0&o=fbw=gwrk0avc55m#k1hcvs&__t_&^'

# GOOGLE_APIKEY = 'AIzaSyB_GcFymLLde7slDgoMNRHRgeWMSpOKp4s'
GOOGLE_APIKEY = 'AIzaSyDlkHz8nwNGtbzLgpLU8yGcrG8KIEwv07s'


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = [
    'kareem.juniorgeorgy.webfactional.com',
    'www.kareem.juniorgeorgy.webfactional.com',
]

# Application definition

INSTALLED_APPS = [
    'django.contrib.sites',
    'dal',
    'dal_select2',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'main',
    'authentication',
    'postman',
    'ajax_select',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.google',
    'autofixture',
    'comments',
    'ckeditor',
    'mailer',
]

SITE_ID = 1

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'myproject.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                # allauth

            ],
        },
    },
]

WSGI_APPLICATION = 'myproject.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',

    # `allauth` specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',
)

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/z

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = False

USE_L10N = False

USE_TZ = False


LOGIN_URL = '/auth/user/login'

LOGIN_REDIRECT_URL = '/profiles/home'

APP_NAME = "SoCar"

if DEPLOYED:
    DOMAIN = ALLOWED_HOSTS[0]
else:
    DOMAIN = "localhost:8000"

# Email configuration

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST = 'smtp.gmail.com'

EMAIL_HOST_USER = 'socialcar.info@gmail.com'

EMAIL_HOST_PASSWORD = 'MadaraUchiha04'

EMAIL_PORT = 587

EMAIL_USE_TLS = True

APP_NAME = 'SoCar'

DOMAIN = 'localhost:8000'

DEFAULT_FROM_EMAIL = 'SoCar@student.guc.edu.eg'


###########################################

# EMAIL_BACKEND = "mailer.backend.DbBackend"

# ADMIN_EMAIL = 'alexan.nader@gmail.com'

# EMAIL_HOST = 'smtp.webfaction.com'  # host used to send mail.

# EMAIL_HOST_USER = 'dreidev_info'

# DEFAULT_FROM_EMAIL = SERVER_EMAIL = 'info@dreidev.com'

# EMAIL_HOST_PASSWORD = 'passo'

# EMAIL_PORT = 587  # port relative to host

# EMAIL_USE_TLS = True  # secure connection


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'

# Django-Postman

POSTMAN_DISALLOW_ANONYMOUS = True

POSTMAN_AUTO_MODERATE_AS = True

POSTMAN_SHOW_USER_AS = lambda u: u.username


AJAX_LOOKUP_CHANNELS = {

    # 'users': dict(model='auth.user', search_field='username'),
    'users': ('main.lookups', 'UserLookup'),

}


POSTMAN_AUTOCOMPLETER_APP = {
    'arg_default': 'users',
}
# FACEBOOK_APP_ID = '1703611953231033'

# FACEBOOK_API_SECRET = '70b208cfd1cc4a610386549b1df13634'

SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        # SCOPE is elements returned from facebook.
        'SCOPE': ['email', 'public_profile', 'user_friends'],
        'AUTH_PARAMS': {'auth_type': 'reauthenticate'},
        'METHOD': 'oauth2',
        'FIELDS': [
            'id',
            'email',
            'name',
            'first_name',
            'last_name',
            'verified',
            'locale',
            'timezone',
            'link',
            'gender',
            'updated_time',
            'friends'],
        'VERIFIED_EMAIL': False
    },

    'google':
    {'SCOPE': ['profile', 'email'],
     'AUTH_PARAMS': {'access_type': 'online'}}
}

# Django-ajax-comments

COMMENTS_ALLOW_LIKES = False
if DEPLOYED:
    STATIC_ROOT = os.path.join(BASE_DIR, '../../socar_static')
    MEDIA_ROOT = os.path.join(STATIC_ROOT, "media")
else:
    MEDIA_URL = '/media/'
    MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')
