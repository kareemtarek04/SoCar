from django import forms
from dal import autocomplete
from functools import partial
import models
# import dal
# from django.utils import timezone
import datetime
DateInput = partial(
    forms.DateInput, {'class': 'datepicker', 'placeholder': 'Date'})
TimeInput = partial(
    forms.TimeInput, {'class': 'timepicker', 'placeholder': 'Time'})


class ProfileForm(forms.ModelForm):
    """
    Profile Form.
    """

    def __init__(self, *args, **kwargs):

        super(ProfileForm, self).__init__(*args, **kwargs)

        # Form Class.
        for key in self.fields:
            self.fields[key].widget.attrs['class'] = 'form-control'

        self.fields['avatar'].required = False

    class Meta:
        model = models.Profile
        exclude = ('user', 'slug', 'location')


class TripForm(forms.ModelForm):
    """
    Trip Form.

    """

    def __init__(self, *args, **kwargs):

        super(TripForm, self).__init__(*args, **kwargs)

        # Hide labels.
        # Add form class.
        for key in self.fields:
            if key != "communities":
                self.fields[key].label = ''
            else:
                self.fields[key].label = 'Restrict To Community'
            self.fields[key].widget.attrs['class'] = 'form-control'

        # Fields required.
        # Those fields are automatically filled by GoogleMapsAPI.
        # self.fields['restrict_to_women'].label = 'Restrict To Women'

        self.fields['source_longitude'].required = False
        self.fields['source_latitude'].required = False
        self.fields['destination_longitude'].required = False
        self.fields['destination_latitude'].required = False

        # Error message.
        self.fields['name'].error_messages = {
            'required': 'Please give your trip a name'}
        self.fields['source_area'].error_messages = {
            'required': 'Please choose Source'}
        self.fields['destination_area'].error_messasges = {
            'required': 'Please choose Destination'}
        self.fields['date'].error_messages = {
            'required': 'Please give your trip a date'}
        self.fields['time'].error_messages = {
            'required': 'Please give your trip a time'}

    def clean_guests(self):
        """
        Validates that guests are always greater than zero.
        """
        data = self.cleaned_data['guests']
        if data < 0:
            raise forms.ValidationError(
                "Guests should be greater than 0")
        if data > 5:
            raise forms.ValidationError(
                "Guests should be less than 5")
        return data

    def clean_date(self):
        """
        Validates that date isn't in the past.
        """

        data = self.cleaned_data['date']
        if data < datetime.date.today():
            raise forms.ValidationError("Date should be in the future")
        return data

    class Meta:
        model = models.Trip
        exclude = ('driver', 'slug')
        widgets = {

            'communities': autocomplete.ModelSelect2Multiple(url='community-autocomplete'),

            # Place holders.
            'name': forms.TextInput(attrs={'placeholder': 'Name'}),
            # 'source_area': forms.TextInput(attrs={'placeholder': 'Source area', 'readonly':True}),
            # 'destination_area': forms.TextInput(attrs={'placeholder': 'Destination area', 'readonly':True}),
            'source_area': forms.TextInput(attrs={'placeholder': 'Source area'}),
            'destination_area': forms.TextInput(attrs={'placeholder': 'Destination area'}),
            # Hidden fields.
            'source_longitude': forms.HiddenInput(),
            'source_latitude': forms.HiddenInput(),
            'destination_longitude': forms.HiddenInput(),
            'destination_latitude': forms.HiddenInput(),
            'source_city': forms.HiddenInput(),
            'destination_city': forms.HiddenInput(),
            # 'distance': forms.HiddenInput(),

            # Date/Time pickers.
            'date': DateInput(),
            'time': TimeInput(),

        }


class GuestForm(forms.ModelForm):
    """
    Trip Guest Form.
    """

    class Meta:
        model = models.Guest
        exclude = ('accepted',)


class FriendRequestForm(forms.ModelForm):
    """
    Friend Request Form.
    """

    class Meta:
        model = models.Friend
        exclude = ('accepted',)


class GroupForm(forms.ModelForm):
    """
    Closed Group Form.
    """

    def __init__(self, *args, **kwargs):

        super(GroupForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].widget.attrs['class'] = 'form-control'

        # Change error message
        self.fields['name'].error_messages = {
            'required': 'Group must have a name'}
        self.fields['members'].error_messages = {
            'required': 'At least add one Member'}

    class Meta:
        model = models.Group
        exclude = ('slug', 'created_by', 'trip')
        widgets = {
            # 'members': dal_select2.Select2WidgetMixin(url='friend-autocomplete'),
            # 'members': dal_select2.widgets.Select2WidgetMixin(),
            # dal.widgets.WidgetMixin(url='friend-autocomplete', forward=None),
            # 'name': autocomplete_light.WidgetMixin(url='friend-autocomplete'),
            'members': autocomplete.ModelSelect2Multiple(url='friend-autocomplete'),
        }


class CommunityForm(forms.ModelForm):
    """
    Closed Community Form.
    """

    def __init__(self, *args, **kwargs):

        super(CommunityForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].widget.attrs['class'] = 'form-control'

        # Change error message
        self.fields['name'].error_messages = {
            'required': 'Group must have a name'}
        self.fields['members'].error_messages = {
            'required': 'At least add one Member'}

    class Meta:
        model = models.Community
        exclude = ('slug', 'created_by')
        widgets = {
            'members': autocomplete.ModelSelect2Multiple(url='friend-autocomplete'),
        }


class FeedbackForm(forms.ModelForm):
    """
    Feedback Form.
    """
    class Meta:
        model = models.Feedback
        exclude = ('user', 'session',)


class LocationForm(forms.ModelForm):
    """
    Location Form.
    """

    def __init__(self, *args, **kwargs):

        super(LocationForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].widget = forms.HiddenInput()

    class Meta:
        model = models.Location
        exclude = ('',)
