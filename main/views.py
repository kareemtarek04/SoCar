# from django.conf import settings
from .models import Trip, Friend, Group, Guest, Profile, UrlHitCounter, ObjectHitCounter, Community, Location
from django.views.generic.edit import FormView, ModelFormMixin
from django.core.urlresolvers import reverse_lazy, reverse
from .forms import (TripForm, FriendRequestForm, GroupForm, CommunityForm, LocationForm,
                    GuestForm, ProfileForm, FeedbackForm)
from django.views.generic import ListView, DetailView
from django.contrib.auth.models import User
from django.db.models import Q
from django.views.generic.edit import UpdateView, DeleteView
from allauth.socialaccount.models import SocialAccount
import json
import helpers
from authentication.views import LoginRequiredMixin
from django.shortcuts import redirect
from django.http import JsonResponse
from django.middleware.csrf import get_token
from django.shortcuts import render
from django.db.models import F
from django.http import Http404
import re
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.template import RequestContext

# from django.core.mail import send_mail


class LoginRedirectMixin(LoginRequiredMixin):
    login_url = '/auth/user/login'
    redirect_field_name = ''


class AjaxableResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'success': 0,
                'error': form.errors})
        else:
            return super(AjaxableResponseMixin, self).form_invalid(form)

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        if self.request.is_ajax():
            form.save()
            return JsonResponse({
                'success': 1, 'csrf': get_token(self.request)})
        else:
            return super(AjaxableResponseMixin, self).form_valid(form)


@login_required
def home(request):
    # redirect to profile post login.
    return HttpResponseRedirect(reverse('profile', kwargs={'pk': request.user.id}))


class ListWithFormMixin(LoginRedirectMixin,
                        ModelFormMixin, ListView):
    """
    Default get and post functions for ListView.
    """

    def get(self, request, *args, **kwargs):
        self.object = None
        self.form = self.get_form(self.form_class)
        return ListView.get(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class LandingePageView(ListView):
    model = User
    template_name = 'main/index.html'

    def get(self, request, *args, **kwargs):
        # send_mail('Subject here', 'Here is the message.', 'kareem.tarek04@gmail.com', ['kareemtarek_93@hotmail.com'], fail_silently=False)
        # Count number landing page visted.
        # helpers.dismiss_trip(Trip.objects.get(id=7))
        session_key = self.request.session.session_key
        try:
            UrlHitCounter.objects.get(session_key=session_key)
        except:
            UrlHitCounter.objects.create(session_key=session_key)
        return super(LandingePageView, self). get(self, request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(LandingePageView, self).get_context_data(**kwargs)
        context['visits'] = UrlHitCounter.objects.all().count()
        context['object_hit_count'] = ObjectHitCounter.objects.get(id=1)
        return context


class TripCreateView(LoginRedirectMixin, AjaxableResponseMixin, FormView):
    """"
    Trip Create View.

    To do: 1.pass Google Maps API key, 2.Trip's
    name/user composite key
    3. objects.latest to be altered to get id from form
    """

    form_class = TripForm
    template_name = "main/trip_create.html"
    success_url = reverse_lazy('trip-list')

    def form_valid(self, form):
        f = form.save(commit=False)
        f.driver = self.request.user
        f.save()
        # save communities if any
        form.save_m2m()
        # update trip hit.
        helpers.update_hit("trip")
        # create respective closed group.
        name = form.instance.name + " Closed Group"
        group = Group.objects.create(name=name, trip=Trip.objects.latest('id'), created_by=self.request.user)
        # add first participant, trip's author
        group.members.add(self.request.user)
        return super(TripCreateView, self).form_valid(form)


class TripListView(AjaxableResponseMixin, ListWithFormMixin):
    """
    TripListView.
    """
    model = Trip
    form_class = GuestForm
    success_url = reverse_lazy('trip-list')

    def get_context_data(self, **kwargs):
        context = super(TripListView, self).get_context_data(**kwargs)
        context['current_user'] = self.request.user
        context['trip_guest_zip'] = helpers.fetch_trip_guest_zip_list(self.request.user,
                                                                      Trip.objects.all().exclude(driver=self.request.user))
        context['my_trip_guest_zip'] = helpers.fetch_trip_guest_zip_list(self.request.user, Trip.objects.filter(driver=self.request.user))
        # context['visits'] = UrlHitCounter.objects.all().count()
        # context['object_hit_count'] = ObjectHitCounter.objects.get(id=1)
        return context


class TripFilterView(AjaxableResponseMixin, ListWithFormMixin):
    """
    Filter trip with respect to date, gender.
    """
    model = Trip
    form_class = GuestForm
    success_url = reverse_lazy('trip-list')

    def get(self, request, *args, **kwargs):

        # fetch trip with respect to date.
        self.object = None
        self.object_list = None
        date = request.GET.get('date')
        context = super(TripFilterView, self).get_context_data(**kwargs)
        if date:
            context['trip_guest_zip'] = helpers.fetch_trip_guest_zip_list_date(date, self.request.user,
                                                                               Trip.objects.all().exclude(driver=self.request.user))
        else:
            context['trip_guest_zip'] = helpers.fetch_trip_guest_zip_list_women(self.request.user,
                                                                                Trip.objects.all().exclude(driver=self.request.user))

        html = render_to_string("main/offered_trip_list.html", context, context_instance=RequestContext(request))
        return HttpResponse(html)

    # def get_context_data(self, **kwargs):
    #     context = super(TripFilterView, self).get_context_data(**kwargs)
    #     # fetch trip with respect to date.
    #     date = self.request.GET.get('date')
    #     if date:
    #         context['trip_guest_zip'] = helpers.fetch_trip_guest_zip_list_date(date, self.request.user,
    #                                                                            Trip.objects.all().exclude(driver=self.request.user))
    #     else:
    #         context['trip_guest_zip'] = helpers.fetch_trip_guest_zip_list_women(self.request.user,
    #                                                                             Trip.objects.all().exclude(driver=self.request.user))

    #     return context


class TripDetailView(LoginRedirectMixin, DetailView):
    """
    Trip Detailed View.
    """
    model = Trip


class TripUpdateView(LoginRedirectMixin, UpdateView):
    """
    Trip Update View.
    """

    model = Trip
    form_class = TripForm
    template_name_suffix = '_update_form'
    success_url = reverse_lazy('trip-list')

    def get_context_data(self, **kwargs):
        context = super(TripUpdateView, self).get_context_data(**kwargs)
        context['requests'] = Guest.objects.filter(
            trip_id=self.kwargs['pk'], accepted=False)
        context['participants'] = helpers.fetch_trip_accepted_requests(
            Trip.objects.get(id=self.kwargs['pk']))
        context['guest_form'] = GuestForm
        return context


class TripDeleteView(LoginRedirectMixin, DeleteView):
    """
    Trip End.

    Statistics are to be sent to participants.
    """
    model = Trip
    success_url = reverse_lazy('trip-list')


class TripRequestList(ListWithFormMixin):
    """
    Trip Request List.

    To do: make sure view is only accessed by trip user.
    """
    model = Guest
    form_class = GuestForm
    template_name = 'main/trip_request_list.html'

    # def get_queryset(self):
    #     # trip not accepted requests.
    #     queryset = Guest.objects.filter(
    #         trip_id=self.kwargs['pk'], accepted=False)
    #     return queryset

    # def get_context_data(self, **kwargs):
    #     context = super(TripRequestList, self).get_context_data(**kwargs)
    #     # Trip participants/guests.
    #     context['participants'] = helpers.fetch_trip_accepted_requests(
    #         Trip.objects.get(id=self.kwargs['pk']))
    #     return context

    def form_valid(self, form):
        form.save(commit=False)
        trip = Trip.objects.get(id=self.request.POST.get('trip'))
        guest = User.objects.get(id=self.request.POST.get('guest'))
        request = Guest.objects.filter(
            trip=trip, guest=guest)
        if self.request.is_ajax():
            if self.request.POST['value'] == "_accept":
                request.update(accepted=True)
                # update available guests.
                trip.guests = F('guests') - 1
                trip.save(update_fields=['guests'])
                # update relevant group with new member.
                group = Group.objects.get(trip=trip)
                group.members.add(guest)
            elif self.request.POST['value'] == "_reject" or self.request.POST['value'] == "_remove_participant":
                request.delete()
            return JsonResponse({
                'success': 1, 'csrf': get_token(self.request)})
        else:
            return redirect(reverse_lazy('trip-request-list',
                                         kwargs={'pk': self.kwargs['pk'], }))

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'success': 0,
                'error': form.errors})
        else:
            return super(RequestListView, self).form_invalid(form)


class UserListView(AjaxableResponseMixin, ListWithFormMixin):
    """
    SoCar User List
    Users not friends yet.

    To do: implement ajax autocomplete.

    View is to be changed to "People You May Know" in later releases.
    """
    model = User
    template_name = 'main/user_list.html'
    form_class = FriendRequestForm
    success_url = reverse_lazy('user-list')

    # def get_context_data(self, **kwargs):
    #     context = super(UserListView, self).get_context_data(**kwargs)
    #     if self.request.user.is_authenticated():
    #         context['current_user'] = self.request.user
    #     return context

    # def get_queryset(self):
    #     queryset = super(UserListView, self).get_queryset()
    # remove current user
    #     queryset = queryset.filter(~Q(id=self.request.user.id))
    # sent requests
    #     user_requests = helpers.fetch_friend_requests(self.request.user)
    # exclude requests from list of users to be displayed.
    #     for user in queryset:
    #         if user in user_requests:
    #             queryset = queryset.exclude(pk=user.id)
    #     return queryset


class ConnectionsView(AjaxableResponseMixin, ListWithFormMixin):
    """

    View responsible for rendering user's friends, requests
    and give suggestions about people near him.
    """
    model = Friend
    template_name = 'main/connection_list.html'
    form_class = FriendRequestForm
    success_url = reverse_lazy('connections')

    def get_context_data(self, **kwargs):
        context = super(ConnectionsView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            context['current_user'] = self.request.user
            # get people near user
            # people_near_queryset = helpers.fetch_nearby_users(self.request.user)
            people_near_queryset = helpers.fetch_nearby_users(User.objects.get(id=10))
            # GET FACEBOOK FRIENDS.
            friend_request_queryset = Friend.objects.all()
            extra_data = SocialAccount.objects.filter(user=self.request.user).values_list(
                'extra_data', flat=True)
            # extract friend's Facebook id's.
            # extra_data sub listing level paging, next, data, id in list
            # parse JSON to python.
            if extra_data:
                parsed_data = json.dumps(extra_data[0])
                parsed_data = json.loads(parsed_data)
                # SoCar users.
                friend_list = parsed_data['friends']['data']
                corresponding_id_list = []
                for friend in friend_list:
                    corresponding_id_list.append(friend['id'])
                # User ids already part of SoCar.
                friend_add_pk = []
                # Users to invite to SoCar
                friend_invite = []
                for id in corresponding_id_list:
                    try:
                        # get social account
                        social_account = SocialAccount.objects.get(uid=id)
                        # get corresponding user
                        user = User.objects.get(id=social_account.user_id)
                        # add to list
                        friend_add_pk.append(user.id)
                    except:
                        friend_invite.append(id)
                all_users = User.objects.all()
                friend_add = all_users.filter(pk__in=friend_add_pk)
                # exclude if not in request, meant request has already been sent.
                # for friend in friend_add:
                #     if friend not in people_near_queryset:
                #         friend_add = friend_add.exclude(id=friend.id)
                # # exclude from people_near query avoiding duplication.
                # for friend in friend_add:
                #     if friend in people_near_queryset:
                #         people_near_queryset = people_near_queryset.exclude(id=friend.id)
                context['friend_add'] = friend_add
            people_near_queryset.append(Profile.objects.get(id=7))
            context['people_near'] = people_near_queryset
            context['friend_requests'] = friend_request_queryset.filter(receiver=self.request.user, accepted=False)
            list_nearby_locations = []
            # name_list = []
            for profile in people_near_queryset:
                location = Location.objects.get(id=profile.location_id)
                # user_name = User.objects.get(id=profile.user_id).username
                latlng = location.latitude + "," + location.longitude
                list_nearby_locations.append(latlng)
                # name_list.append(user_name)
            json.dumps(list_nearby_locations)
            context['location_list_json'] = json.dumps(list_nearby_locations)
            # context['name_list'] = json.dumps(name_list)
            # Send current user's location
            # user_profile = Profile.objects.get(user=self.request.user)
            user_profile = Profile.objects.get(user=User.objects.get(id=10))
            user_location = Location.objects.get(id=user_profile.location_id)
            user_location = user_location.latitude + "," + user_location.longitude
            context['user_location'] = json.dumps(user_location)
            return context

    def get_queryset(self):
        # object_list
        return helpers.fetch_friends(self.request.user)


class FriendListView(LoginRedirectMixin, ListView):
    """
    Friends list View.
    """
    model = Friend
    template_name = 'main/friend_list.html'

    # def get_queryset(self):
    #     return helpers.fetch_friends(self.request.user)


class RequestListView(ListWithFormMixin):
    """
    Friend requests list.
    """
    model = Friend
    form_class = FriendRequestForm
    template_name = 'main/friend_request_list.html'

    # def get_context_data(self, **kwargs):
    #     context = super(RequestListView, self).get_context_data(**kwargs)
    #     context['current_user'] = self.request.user
    #     return context

    # def get_queryset(self):
    #     queryset = super(RequestListView, self).get_queryset()
    #     return queryset.filter(receiver=self.request.user, accepted=False)

    def form_valid(self, form):
        form.save(commit=False)
        request = Friend.objects.filter(
            sender=User.objects.get(id=self.request.POST.get('sender')),
            receiver=User.objects.get(id=self.request.POST.get('receiver')))
        if self.request.is_ajax():
            if self.request.POST['value'] == "_accept":
                request.update(accepted=True)
            elif self.request.POST['value'] == "_reject":
                request.delete()
            return JsonResponse({
                'success': 1, 'csrf': get_token(self.request)})
        else:
            return redirect(reverse_lazy('request-list'))

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'success': 0,
                'error': form.errors})
        else:
            return super(RequestListView, self).form_invalid(form)


class GroupListView(LoginRedirectMixin, ListView, FormView):
    """
    Group List View.

    To do:override form.save() to add form.save_m2m().
    Groups' name/created_by composite key.
    """
    form_class = CommunityForm
    success_url = reverse_lazy('group-list')
    model = Community
    template_name = "main/group_list.html"

    def get_queryset(self):
        queryset = super(GroupListView, self).get_queryset()
        # need to add if auth user is one of members
        queryset = queryset.filter(Q(created_by=self.request.user))
        return queryset

    def get_context_data(self, **kwargs):
        context = super(GroupListView, self).get_context_data(**kwargs)
        # Groups created by user are automatically excluded.
        context['my_groups'] = helpers.fetch_group_user_part_of(
            self.request.user)
        return context

    def form_valid(self, form):
        f = form.save(commit=False)
        f.created_by = self.request.user
        helpers.update_hit("community")
        f.save()
        form.save_m2m()
        # add current user to the newly created community.
        community = Community.objects.latest('id')
        community.members.add(self.request.user)
        if self.request.is_ajax():
            return JsonResponse({
                'success': 1, 'csrf': get_token(self.request)})
        else:
            return super(GroupListView, self).form_valid(form)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'success': 0,
                'error': form.errors})
        else:
            return super(GroupListView, self).form_invalid(form)


class GroupDetailView(LoginRedirectMixin, DetailView):

    """
    Group Detailed View.
    """
    model = Group

    def get_context_data(self, **kwargs):
        context = super(GroupDetailView, self).get_context_data(**kwargs)
        trip_list = Trip.objects.all().values_list('name', flat=True)
        trip_list_list = []
        for name in trip_list:
            trip_list_list.append(name)
        context['trip_list'] = json.dumps(trip_list_list)
        return context

    def get(self, request, *args, **kwargs):
        group_participants = helpers.fetch_group_participants(
            self.get_object())
        if self.request.user.id in group_participants or self.request.user == self.get_object().created_by:
            return super(GroupDetailView, self).get(
                self, request, *args, **kwargs)
        else:
            return render(request, 'main/404.html', {}, )


class GroupUpdateView(LoginRedirectMixin, UpdateView):
    """
    Group Update View.
    """

    model = Group
    form_class = GroupForm
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse('group-detail', kwargs={'pk': self.kwargs['pk']})

    def get(self, request, *args, **kwargs):
        if self.request.user == self.get_object().created_by:
            return super(GroupUpdateView, self).get(
                self, request, *args, **kwargs)

        else:
            return render(request, 'main/404.html', {}, )

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.save()
        form.save_m2m()
        return super(GroupUpdateView, self).form_valid(form)


class ProfileDetailView(LoginRedirectMixin, DetailView, FormView):
    """
    Profile Detailed View.
    """
    model = Profile
    form_class = ProfileForm

    def get_object(self):
        if int(float(self.kwargs['pk'])) == int(float(self.request.user.id)):
            try:
                return Profile.objects.get(user=self.request.user)
            except:
                return Profile.objects.create(user=self.request.user)
        else:
            try:
                return Profile.objects.get(user_id=self.kwargs['pk'])
            except:
                raise Http404("Profile does not exist")

    def get_context_data(self, **kwargs):
        context = super(ProfileDetailView, self).get_context_data(**kwargs)
        context['location_form'] = LocationForm
        return context

    def get(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
        except Http404:
            return render(self.request, 'main/404.html', {}, )
        return super(ProfileDetailView, self).get(self, request, *args, **kwargs)

    def get_success_url(self):
        return reverse('profile', kwargs={'pk': self.kwargs['pk']})


class LocationUpdateView(LoginRedirectMixin, FormView):
    """
    Update user's current location
    """
    model = Location
    form_class = LocationForm
    # template_name_suffix = '_update_form'

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        Profile.objects.filter(user_id=self.request.user.id).update(location=form.save())
        return super(LocationUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('profile', kwargs={'pk': self.kwargs['pk']})


class ProfileUpdateView(LoginRedirectMixin, UpdateView):
    """
    Profile Update View.
    """

    model = Profile
    form_class = ProfileForm
    template_name_suffix = '_update_form'

    def get_object(self):
        return Profile.objects.get(user_id=self.kwargs['pk'])

    def get(self, request, *args, **kwargs):

        if self.request.user.id == self.get_object().user.id:
            return super(ProfileUpdateView, self).get(
                self, request, *args, **kwargs)
        else:
            return render(request, 'main/404.html', {}, )

    def get_success_url(self):
        return reverse('profile', kwargs={'pk': self.kwargs['pk']})


class FeedbackView(FormView):
    """
    FeedbackView
    """
    template_name = "main/feedback_form.html"
    form_class = FeedbackForm
    success_url = '/'

    def form_valid(self, form):
        # If authenticated user sent feedback, save user.
        # save session_key for user integrity.
        form.save(commit=False)
        if self.request.user.is_authenticated():
            form.instance.user = self.request.user
        form.instance.session = self.request.session.session_key
        form.save()
        return super(FeedbackView, self).form_valid(form)
