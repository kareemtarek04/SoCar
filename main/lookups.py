from __future__ import unicode_literals
from dal import autocomplete
import helpers
import models
from django.db.models import Q
from django.utils.html import escape
from django.contrib.auth.models import User
from authentication.views import LoginRequiredMixin
# ajax-select
from ajax_select import LookupChannel
# import re


class FriendAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):

    def get_queryset(self):
        pk_list = []
        for user in helpers.fetch_friends(self.request.user):
            pk_list.append(user.id)
        qs = User.objects.filter(pk__in=pk_list)
        if self.q:
            qs = qs.filter(username__istartswith=self.q)
        return qs


class CommunityAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = models.Community.objects.filter(created_by=self.request.user)
        if self.q:
            qs = qs.filter(username__istartswith=self.q)
        return qs

# For Post Man(PM).


class UserLookup(LookupChannel):

    model = User

    def get_query(self, q, request):
        return User.objects.filter(Q(username__icontains=q) | Q(email__istartswith=q)).order_by('username')

    def get_result(self, obj):
        """ result is the simple text that is the completion of what the person typed """
        return obj.username

    def format_match(self, obj):
        """ (HTML) formatted item for display in the dropdown """
        return "%s<div><i>%s</i></div>" % (escape(obj.username), escape(obj.email))
        # return self.format_item_display(obj)

    def format_item_display(self, obj):
        """ (HTML) formatted item for displaying item in the selected deck area """
        return "%s<div><i>%s</i></div>" % (escape(obj.username), escape(obj.email))
