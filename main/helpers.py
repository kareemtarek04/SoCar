from .models import Friend
from django.db.models import Q
import models
from allauth.socialaccount.models import SocialAccount
from django.contrib.auth.models import User
from django.db.models import F
import re
from django.core.mail import send_mail
from myproject.settings import APP_NAME, DOMAIN, DEFAULT_FROM_EMAIL
from django.template import loader


def fetch_friend_requests(user):
    """
    Friend Requests To User.
    """
    sent_requests = Friend.objects.filter(
        Q(sender=user) | Q(receiver=user))
    user_requests = []
    for request in sent_requests:
        if request.sender not in user_requests and request.sender.id != user.id and request.receiver.id == user.id:
            user_requests.append(request.sender)
        if request.receiver not in user_requests and request.receiver.id != user.id and request.sender.id == user.id:
            user_requests.append(request.receiver)
    return user_requests


def fetch_friend_requests_social_account(user):
    sent_requests = Friend.objects.filter(
        Q(sender=user) | Q(receiver=user))
    user_requests = []
    for request in sent_requests:
        if request.sender not in user_requests and request.sender.id != user.id and request.receiver.id == user.id:
            user_requests.append(
                SocialAccount.objects.filter(user_id=request.sender.id)[0])
        if request.receiver not in user_requests and request.receiver.id != user.id and request.sender.id == user.id:
            user_requests.append(
                SocialAccount.objects.filter(user_id=request.receiver.id)[0])
    return user_requests


def fetch_friends(user):
    """
    Friends Of User.
    """
    friends = Friend.objects.filter(
        (Q(sender=user) | Q(receiver=user)), Q(accepted=True))
    queryset = []
    for friend in friends:
        if friend.sender not in queryset and friend.sender.id != user.id and friend.receiver.id == user.id:
            queryset.append(friend.sender)
        if friend.receiver not in queryset and friend.receiver.id != user.id and friend.sender.id == user.id:
            queryset.append(friend.receiver)
    return queryset


def fetch_trip_requests(trip):
    """
    Join Trip Requests, accepted or pending.
    """
    return models.Guest.objects.filter(trip=trip).values_list('guest', flat=True)


def fetch_trip_accepted_requests(trip):

    return models.Guest.objects.filter(
        trip=trip, accepted=True)


def fetch_group_user_part_of(user):
    """
    Return groups user is member of. Excludes trips created by user.
    """
    groups_returned = []
    for group in models.Group.objects.all():
        if group.members.through.objects.filter(group=group, user=user):  # and group.created_by != user:
            groups_returned.append(group)
    return groups_returned


def fetch_group_participants(group):
    return group.members.through.objects.filter(group=group).values_list('user', flat=True)


def fetch_trip_guest_zip_list(current_user, trip_list):
    """
    Returns trips in trip_list associated with its guests as one zipped list.
    """
    guest_lists = []
    not_guest = []
    # filter trips with permission
    trip_list = filter_trips_with_permission(trip_list, current_user)
    for trip in trip_list:
        guest_lists.append(fetch_trip_requests(trip))
    for list in guest_lists:
        if current_user.id not in list:
            not_guest.append(
                current_user)
        else:
            not_guest.append(
                '')
    return zip(trip_list, not_guest)


def fetch_trip_guest_zip_list_date(date, current_user, trip_list):
    """
    Returns trips with respect to date.
    """
    guest_lists = []
    not_guest = []
    pk_list = []
    # filter trips with permission
    trip_list = filter_trips_with_permission(trip_list, current_user)
    for trip in trip_list:
        pk_list.append(trip.id)
    trip_list = models.Trip.objects.filter(pk__in=pk_list, date=date)
    for trip in trip_list:
        guest_lists.append(fetch_trip_requests(trip))
    for list in guest_lists:
        if current_user.id not in list:
            not_guest.append(
                current_user)
        else:
            not_guest.append(
                '')
    return zip(trip_list, not_guest)


def fetch_trip_guest_zip_list_women(current_user, trip_list):
    """
    Returns trips with respect to gender.
    """
    guest_lists = []
    not_guest = []
    pk_list = []
    # filter trips with permission
    trip_list = filter_trips_with_permission(trip_list, current_user)
    for trip in trip_list:
        pk_list.append(trip.id)
    trip_list = models.Trip.objects.filter(pk__in=pk_list, restrict_to_women=True)
    for trip in trip_list:
        guest_lists.append(fetch_trip_requests(trip))
    for list in guest_lists:
        if current_user.id not in list:
            not_guest.append(
                current_user)
        else:
            not_guest.append(
                '')
    return zip(trip_list, not_guest)


def update_hit(hit_object):
    """
    Update hit count for specific models.
    """
    entry = models.ObjectHitCounter.objects.get(id=1)
    if hit_object == "sign_up":
        entry.sign_up_forms = F('sign_up_forms') + 1
        entry.save(update_fields=['sign_up_forms'])
    elif hit_object == "trip":
        entry.trips_created = F('trips_created') + 1
        entry.save(update_fields=['trips_created'])
    elif hit_object == "community":
        entry.groups_created = F('groups_created') + 1
        entry.save(update_fields=['groups_created'])


def filter_trips_with_permission(trip_list, current_user):
    """
    Restrict unauthorized user from viewing trips.
    """
    trip_list_return = []
    for trip in trip_list:
        # print "**** trip ****"
        # print trip
        # get bounded communities
        community_list = trip.communities.through.objects.filter(trip=trip)
        # print "**** community_list ****"
        # print community_list
        # if there a restriction for
        if community_list:
            # fetch every community belong to trip
            for community_entry in community_list:
                # get community from entry.
                community = models.Community.objects.get(id=community_entry.community_id)
                # print "**** community ****"
                # print community

                # print "**** current_user_id ****"
                # print current_user.id

                # print "**** community members ****"
                # print community.members.through.objects.filter(community=community).values_list('user', flat=True)
                # if current user member of community bounded to trip and trip not append, append trip.
                if current_user.id in community.members.through.objects.filter(community=community).values_list('user', flat=True) and trip not in trip_list_return:
                    trip_list_return.append(trip)
        else:
            trip_list_return.append(trip)

    # print "**** trip_list_return ****"
    # print trip_list_return
    return trip_list_return
    # return trip_list


def dismiss_trip(trip):
    # fetch respective guests
    guest_list = models.Guest.objects.filter(trip=trip, accepted=True).values_list('guest', flat=True)
    email_list = []
    # Fetch all concerned mails.
    for user_id in guest_list:
        try:
            email = User.objects.get(id=user_id).email
            email_list.append(email)
        except:
            pass
    # Calculate distance covered.
    dist_covered = trip.distance
    dist_covered = dist_covered.split()
    dist_covered = dist_covered[0]
    dist_covered = float(dist_covered)
    social_interactions = trip.guests + 1
    # Calculate gas saved.
    gas_cost_per_km = 0.3
    cost_round_trip = gas_cost_per_km * dist_covered * 2
    cost_per_month = cost_round_trip * 24
    cost_per_month_guests = cost_per_month / social_interactions
    # Calculate CO emmision
    co_per_km = 0.6875
    co_round_trip = co_per_km * dist_covered
    co_per_month = co_round_trip * 24
    co_per_month_guest = co_per_month / social_interactions
    # Compose message
    msg = "The trip created by " + trip.driver.username + " has been Dismissed, the following are the carpool's statistics along a whole month\n\n"
    msg += "Cost of gas solo driving: " + str(cost_per_month) + " Egyptian pounds. \n\n"
    msg += "Cost of gas with carpooling (present participants): " + str(cost_per_month_guests) + " Egyptian pounds. \n\n"
    msg += "Harmful gases emitted solo driving: " + str(co_per_month) + " pounds\n\n"
    msg += "Harmful gases emitted with carpooling (present participants): " + str(co_per_month_guest) + " pounds\n\n"
    msg += "Social Interactions (trip's participants): " + str(social_interactions) + " people\n\n"
    # Send mail, message's body is to be altered to html.
    html_message = loader.render_to_string(
        'main/conclusion_email.html',
        {
            'trip': trip,
            'conc_trip_month_alone': str(cost_per_month),
            'conc_trip_month_carpool': str(cost_per_month_guests),
            'conc_trip_gas_alone': str(co_per_month),
            'conc_trip_gas_carpool': str(co_per_month_guest),
        })
    send_mail(
        "Trip dismissed notification", msg, DEFAULT_FROM_EMAIL, email_list, fail_silently=False, html_message=html_message)


def fetch_nearby_users(user):
    current_user_profile = models.Profile.objects.get(user=user)
    current_user_loc = models.Location.objects.get(id=current_user_profile.location_id)
    # fetch not friends
    not_friends_list = fetch_not_friends_profile(user)
    near_user_list_profile = []
    for not_friend in not_friends_list:
        try:
            not_friend_location = models.Location.objects.get(id=not_friend.location_id)
            if not_friend.id != current_user_profile.id and location_in_radius(not_friend_location, current_user_loc):
                near_user_list_profile.append(not_friend)
        except:
            pass
    # print " ***** near_user_list_profile *****"
    # print near_user_list_profile
    return near_user_list_profile


def location_in_radius(not_friend_location, current_user_loc):
    north, east = parse_bound_to_tuple(current_user_loc.upper_bound)
    south, west = parse_bound_to_tuple(current_user_loc.lower_bound)
    not_friend_latlng = "(" + not_friend_location.latitude + "," + " " + not_friend_location.longitude + ")"
    latitude, longitude = parse_bound_to_tuple(not_friend_latlng)
    # print (" south ", south)
    # print (" latitude ", latitude)
    # print (" north ", north)
    # print "*"
    # print (" west ", west)
    # print (" longitude ", longitude)
    # print (" east ", east)
    # print " ####################### "
    if (latitude >= south and latitude <= north) and (longitude >= west and longitude <= east):
        # print "TRUE"
        return True
    else:
        # print "FALSE"
        return False

    # return True


def fetch_not_friends_profile(user):
    # users not friends
    not_friends_list = User.objects.all()
    # remove current user
    not_friends_list = not_friends_list.filter(~Q(id=user.id))
    # sent requests
    user_requests = fetch_friend_requests(user)
    # exclude requests from list of users to be displayed.
    for user in not_friends_list:
        if user in user_requests:
            not_friends_list = not_friends_list.exclude(pk=user.id)
    not_friends_list = not_friends_list.values_list('id', flat=True)
    profile_list = models.Profile.objects.all()
    return_profile_list = []
    for prof in profile_list:
        if prof.user_id in not_friends_list:
            return_profile_list.append(prof)
    return return_profile_list


def parse_bound_to_tuple(string):
    p = re.compile(r'\W+')
    split = p.split(string)
    fst_arg = split[1] + "." + split[2]
    fst_arg = float(fst_arg)
    sec_arg = split[3] + "." + split[4]
    sec_arg = float(sec_arg)
    tup = (fst_arg, sec_arg)
    return tup
