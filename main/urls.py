from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static
from .lookups import FriendAutocomplete, CommunityAutocomplete
import views
from django.views.generic import TemplateView

urlpatterns = patterns(
    '',
    url(r'^$', views.LandingePageView.as_view(), name='index'),
    url(r'^trip/list/offered/trips', TemplateView.as_view(template_name="main/offered_trip_list.html")),
    url(r'^profiles/home', views.home, name='home'),
    url(
        r'^trip/filter/$', views.TripFilterView.as_view(),
        name='trip-filter'),
    url(
        r'^trip/new/$', views.TripCreateView.as_view(),
        name='trip-new'),
    url(r'^trip/list/(?P<pk>[-_\w]+)/$', views.TripDetailView.as_view(),
        name='trip-detail'),
    url(r'^trip/request/list/(?P<pk>[-_\w]+)/$',
        views.TripRequestList.as_view(),
        name='trip-request-list'),
    url(r'^trip/list/(?P<pk>[-_\w]+)/update/$', views.TripUpdateView.as_view(),
        name='trip-update'),
    url(r'^trip/list/(?P<pk>[-_\w]+)/delete/$', views.TripDeleteView.as_view(),
        name='trip-delete'),
    url(r'^trip/list/$', views.TripListView.as_view(), name='trip-list'),
    url(r'^user/list/$', views.UserListView.as_view(), name='user-list'),
    url(r'^requests/$', views.RequestListView.as_view(),
        name='request-list'),
    url(r'^friend/list$', views.FriendListView.as_view(),
        name='friend-list'),
    # url(r'^facebook/user/list/$', views.FacebookUserListView.as_view(),
    #     name='facebook-user-list'),
    url(r'data/$', 'data.test_data', name='populate'),
    # url(r'^group/create/$', views.GroupCreateView.as_view(),
    #     name='group-new'),
    url(r'^group/list/$', views.GroupListView.as_view(), name='group-list'),
    url(r'^group/list/(?P<pk>[-_\w]+)/$', views.GroupDetailView.as_view(),
        name='group-detail'),
    url(r'^group/list/(?P<pk>[-_\w]+)/update/$',
        views.GroupUpdateView.as_view(),
        name='group-update'),
    url(
        r'^friend-autocomplete/$',
        FriendAutocomplete.as_view(),
        name='friend-autocomplete',
    ),
    url(
        r'^community-autocomplete/$',
        CommunityAutocomplete.as_view(),
        name='community-autocomplete',
    ),
    url(r'^profile/(?P<pk>[-_\w]+)/$', views.ProfileDetailView.as_view(),
        name='profile'),
    url(r'^profile/(?P<pk>[-_\w]+)/update/$',
        views.ProfileUpdateView.as_view(),
        name='profile-update'),
    url(r'^location/(?P<pk>[-_\w]+)/update/$',
        views.LocationUpdateView.as_view(),
        name='location-update'),
    url(r'^feedback/submit/$',
        views.FeedbackView.as_view(),
        name='feedback-submit'),
    url(r'^connections/$',
        views.ConnectionsView.as_view(),
        name='connections'),


) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += patterns('',
                            url(r'^media/(?P<path>.*)$',
                                'django.views.static.serve', {
                                    'document_root': settings.MEDIA_ROOT,
                                }),
                            url(r'^static/(?P<path>.*)$',
                                'django.views.static.serve', {
                                    'document_root': settings.STATIC_ROOT,
                                }),
                            )
