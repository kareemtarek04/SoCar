$(document).ready( function() {
	$('#id_comment').textcomplete([
	{ // html
	    mentions: fetch_trip_names(),
	    match: /\B@(\w*)$/,
	    search: function (term, callback) {
	        callback($.map(this.mentions, function (mention) {
	            return mention.indexOf(term) === 0 ? mention : null;
	        }));
	    },
	    index: 1,
	    replace: function (mention) {
	        return '@' + mention ;
	    }
	}
	]);

	function fetch_trip_names(){
		var objJSON = jQuery.parseJSON($("input[name=trip_name_list]").val());
		var arr = [];
		for (var i = 0, len = objJSON.length; i < len; ++i) {
		var str = "" +objJSON[i]
		arr.push(str)
		}
		return arr;

	}
});