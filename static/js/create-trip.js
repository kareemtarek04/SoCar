$(document).ready( function() {
	var frm = $('#trip-create');
	var error = document.getElementById('trip-create-errors');
	frm.submit(function () {
	    $.ajax({
		    type: "POST",
		    url: "",
		    data: frm.serialize(),
		    success: function (json) {
                if(json.success == 1) {
                    alert("Trip's now posted !");
                    window.location = '/trip/list' 
                } else if(json.success == 0){
                     errors = ""
                  for (var err in json.error){
                    errors += "" + json.error[err] + "\n" +"<br> <br>";
                }
                error.innerHTML = errors;
            	}
		    },
		    error: function(data) {
		    alert("Oops ! something went wrong.");
		        },
	    });
	    return false;
	});
});


