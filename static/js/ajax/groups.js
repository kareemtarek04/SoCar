$(document).ready( function() {
	var frm = $('#group-create');
	var error = document.getElementById('group-create-errors');
	frm.submit(function () {
	    $.ajax({
		    type: "POST",
		    url: "/group/list/",
		    data: frm.serialize(),
		    success: function (json) {
                if(json.success == 1) {
                    alert(" Community Created Successfully !");
                } else if(json.success == 0){
                     errors = ""
                  for (var err in json.error){
                    errors += "" + json.error[err] + "\n" +"<br> <br>";
                }
                error.innerHTML = errors;
            	}
		    },
		    error: function(data) {
		    alert("Oops ! something went wrong.");
		        },
	    });
	    return false;
	});
});


