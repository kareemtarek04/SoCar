$(document).ready( function() {
	var object_id = $("[name='update-profile-button']").attr("object");

	$("[name='update-profile-button']").click(function(){
		$( ".contact-form" ).hide();
		$( ".profile-form" ).empty();
		url = "/profile/"+object_id+"/update/"
		$.ajax({
	            url: url,
	            type: "GET",
	            processData: false,
	            contentType: false,
	            success: function(html) {
	            	$( ".profile-form" ).append(html);
	            },
	            error: function(response) {
	            	alert("Oops ! something went wrong."); 
	            }
        }); 
        $( ".profile-form" ).show();
	});

	$("[name='update-location-button']").click(function(){
		$( ".profile-form" ).hide();
		$( ".contact-form" ).show();
	});
});