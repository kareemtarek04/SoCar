$(document).ready( function() {
	$("[name='friend-request-button']").click(function(){
		var object_id = $(this).attr("object");
		var form = ".friend-request-form"+object_id ;
		var button = '#friend-request-button'+object_id;
		var data =  new FormData($(form).get(0));
		$.ajax({
	            url: "/user/list/",
	            type: "POST",
	            data: data,
	            processData: false,
	            contentType: false,
	            success: function(json) {
	            	if (json.success == 0) {
                  		alert("Oops ! something went wrong."); 
                	}
	            	else if (json.success == 1){
		            	$(button).attr('value', 'Request Sent');
		                $("input[name=csrfmiddlewaretoken]").val(json.csrf);            	
						}
	            },
	            error: function(response) {
	            	alert("Oops ! something went wrong."); 
	            }
        }); 
	});

	$("[name='friend-button']").click(function(){
		var object_id = $(this).attr("object");
		var form = ".friend-form"+object_id ;
		var button_value = $(this).val();
		var data =  new FormData($(form).get(0));
		var accept_button = "#accept-friend-button"+object_id
		var reject_button = "#reject-friend-button"+object_id
		if(button_value == "Accept") 
       		data.append('value',"_accept");
       	else
       		data.append('value',"_reject");
		$.ajax({
	            url: "/requests/",
	            type: "POST",
	            data: data,
	            processData: false,
	            contentType: false,
	            success: function(json) {
	            	if (json.success == 0) {
                  		alert("Oops ! something went wrong."); 
                	}
	            	else if (json.success == 1){
					    if(button_value == "Accept"){
					    	$(accept_button).attr('value', 'Request Accepted');
					    	$(reject_button).hide();
					    }
				       	else{
				       		$(reject_button).attr('value', 'Request Rejected');
				       		$(accept_button).hide();
				       	}
				     	
		                $("input[name=csrfmiddlewaretoken]").val(json.csrf);            	
						}
	            },
	            error: function(response) {
	            	alert("Oops ! something went wrong."); 
	            }
        }); 
	});

});


/*



	<form action="" method="POST" class="friend-request-form{{user.id}}">
					{% csrf_token %}
					<select id="id_receiver" name="receiver" hidden>
						<option value="{{user.id}}" selected="selected" ></option>
					</select>
					<select id="id_sender" name="sender" hidden>
						<option value="{{current_user.id}}" selected="selected" ></option>
					</select>
					<input  id="friend-request-button{{user.id}}" type="button" class="friend-request-button" value="Send Friend Request " /> <br><br>

	</form>


*/

/*
<form action="" method="POST" class="guest-form{{not_guest.id}}">
						{% csrf_token %}
						<select id="id_guest" name="guest" hidden>
						<option value="{{not_guest.id}}" selected="selected" ></option>
						</select>
						<select id="id_trip" name="trip" hidden>
						<option value="{{trip.id}}" selected="selected" ></option>
						</select>
						<input  id="join-trip-button{{not_guest.id}}" type="button" value="Request to Join Trip" class="join-trip-button" object="{{not_guest.id}}"/> <br><br>
</form>*/