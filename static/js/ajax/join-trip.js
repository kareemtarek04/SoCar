$(document).ready( function() {
	$("[name='join-trip-button']").click(function(){
		var object_id = $(this).attr("object");
		var form = ".guest-form"+ object_id ;
		var button = '#join-trip-button'+object_id;
		var data =  new FormData($(form).get(0));
		$.ajax({
	            url: "/trip/list/",
	            type: "POST",
	            data: data,
	            processData: false,
	            contentType: false,
	            success: function(json) {
	            	if (json.success == 0) {
                  		alert("Oops ! something went wrong."); 
                	}
	            	else if (json.success == 1){
		            	$(button).attr('value', 'Request Sent');
		                $("input[name=csrfmiddlewaretoken]").val(json.csrf);            	
						}
	            },
	            error: function(response) {
	            	alert("Oops ! something went wrong."); 
	            }
        }); 
	});

});

