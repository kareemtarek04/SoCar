$(document).ready( function() {
    var map;
    initialize_map();
    parse_nearby_locations();
});

function initialize_map(){
      center = fetch_user_location(); 
      zoom_level = 11,
      // map initialization.
      map = new google.maps.Map(document.getElementById('map'),{
      center: center,
      zoom: zoom_level,
      scrollwheel: false,
      navigationControl: false,
      mapTypeControl: false,
      scaleControl: false,
      draggable: false,
      zoomControl: false,
      disableDoubleClickZoom: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      // summon current location marker
      summon_marker(center, true);
      // draw radius circle
      draw_circle(center);
    }

    function fetch_user_location(){
      var objJSON = jQuery.parseJSON($("input[name=user_location_input]").val());
      latlong = ""+objJSON
      latlong = latlong.split(',')
      lat = parseFloat(latlong[0]);
      long = parseFloat(latlong[1]);
      latlong = new google.maps.LatLng(lat,  long);
      // set map center to user's current location
      return latlong;


    }

    function parse_nearby_locations(){
      var objJSON = jQuery.parseJSON($("input[name=people_near_input]").val());
/*      var name_list = jQuery.parseJSON($("input[name=name_list]").val());
*/
      for (var i = 0, len = objJSON.length; i < len; ++i) {
       var latlong = objJSON[i];
       latlong = ""+latlong
       latlong = latlong.split(',')
       lat = parseFloat(latlong[0]);
       long = parseFloat(latlong[1]);
       latlong = new google.maps.LatLng(lat,  long);
/*       name = "" + name_list[i]
*/       summon_marker(latlong,false);

     }
    }


    function summon_marker (latlong, bool){
      // bool is true if user current location
      var image;
      if (bool){
        image = '/static/images/maps/house.png';
      }
      else{
        image = '/static/images/maps/male.png';
      }
      marker = new google.maps.Marker({
        position: latlong,
        map: map,
        animation: google.maps.Animation.DROP,
        icon: image,
      });
     
    if(bool){
        var infowindow = new google.maps.InfoWindow({ 
        content:"You're Here",
        });
        infowindow.open(map,marker);
      }

    }

      // initial function.
    function draw_circle(loc){
      circle = new google.maps.Circle({
        map: map,
        clickable: false,
        // metres
        radius: 13000,
        fillColor: '#82A0FA',
        fillOpacity: .4,
        strokeColor: '#82A0FA',
        strokeOpacity: .9,
        strokeWeight: .8,
        center:loc,
      });
     
    }