/////////////////////// form elements ///////////////////////}
/////////////////////// initial global variables ///////////////////////

$(document).ready( function() {

 	cairo = new google.maps.LatLng(30.064742,  31.249509); 
    circle = null;
    zoom_level = 11
// map initialization.
    map = new google.maps.Map(document.getElementById('map'),{
	center: cairo,
	zoom: zoom_level,
	mapTypeId: google.maps.MapTypeId.ROADMAP
	});



// markers initialization.
	marker_2 = new google.maps.Marker({
			position:cairo,
			map: map,
			draggable:true,
			animation: google.maps.Animation.DROP
	}); 

	var infowindow = new google.maps.InfoWindow({ 
			content:"Drag marker to location"
	});
	infowindow.open(map,marker_2); 
// markers' listeners
	google.maps.event.addListener(marker_2, 'dragend', function (event) {
		circle.setMap(null);
		initial(marker_2.getPosition());
		map.setZoom(11);
	});

	GelocationService(marker_2);

	// Search box attributes
	// Create the search box and link it to the UI element.
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Listen for the event fired when the user selects a prediction and retrieve
	// more details for that place.

	searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

    // Create a marker for each place.
	circle.setMap(null);
	initial(place.geometry.location);
	if (place.geometry.viewport) {
	// Only geocodes have viewport.
	bounds.union(place.geometry.viewport);
	} else {
	bounds.extend(place.geometry.location);
	}
    });
    map.fitBounds(bounds);
    map.setZoom(11);
  });

	



});
  // detailed information.

    function reverseGecoding(location){
		var lat = location.lat();
		var lng = location.lng();
		var xmlHttp = null;

		//initialize request and send it
		xmlHttp = new XMLHttpRequest();
		xmlHttp.open( "GET", "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&key=AIzaSyDlkHz8nwNGtbzLgpLU8yGcrG8KIEwv07s", false );
		xmlHttp.send( null );

		//calls a function to process the response that was received from the request
		ProcessRequest(xmlHttp.responseText);
	}

//takes as an input a JSON object containing location details
	function ProcessRequest(response) {
		$("#id_latitude").attr('value',  marker_2.getPosition().lat());
	    $("#id_longitude").attr('value',  marker_2.getPosition().lng());

		//parse json object to a dictionary
		var json_data = JSON.parse(response);

		//get the formatted address from the dictionary
		var lol = json_data.results[0]['formatted_address'];

		//splits the string into fields and store it in an array
		var add = lol.split(',');
		$("#id_area").attr('value', add[0]+","+add[1]+","+add[2]);
		$("#input").attr('value', add[0]+","+add[1]+","+add[2]);
	}

	function GelocationService(marker_2) {


		// Try HTML5 geolocation.
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			var infoWindow = new google.maps.InfoWindow({map: map});
			
			var loc = new google.maps.LatLng(pos.lat, pos.lng);
			initial(loc);
			});
		} else {
			alert("----- error -----")			
		}

	}

// initial function.
	function draw_circle(){
		loc = marker_2.getPosition();
		circle = new google.maps.Circle({
	    map: map,
	    clickable: false,
	    // metres
	    radius: 13000,
	    fillColor: '#82A0FA',
	    fillOpacity: .4,
	    strokeColor: '#82A0FA',
	    strokeOpacity: .9,
	    strokeWeight: .8,
	    center:loc,
		});
		$("#id_upper_bound").attr('value', "" + circle.getBounds().getNorthEast());
		$("#id_lower_bound").attr('value', circle.getBounds().getSouthWest());
	}

	function initial(location){
		marker_2.setPosition(location);
		reverseGecoding(marker_2.getPosition());
		draw_circle();

		
	}