from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


class Authentication(TestCase):
    """
    Class to test Authentication system
    """

    def test_sign_up_correct_info(self):
        """
        Tests user sign up with correct info and matching passwords
        """
        self.client.post(
            reverse('user-new'), {'username': 'johndoe',
                                  'email': 'johndoe@gmail.com',
                                  'password1': '1234',
                                  'password2': '1234',
                                  'first_name': 'johndoe'})
        self.assertTrue(User.objects.filter(username='johndoe'))

    def test_sign_up_missmatcing_password(self):
        """
        Tests user sign up with missmatching passwords
        """
        self.client.post(
            reverse('user-new'), {'username': 'johndoe',
                                  'email': 'johndoe@gmail.com',
                                  'password1': '1234',
                                  'password2': '5678',
                                  'first_name': 'johndoe'})
        self.assertFalse(User.objects.filter(username='johndoe'))
