from django.contrib.auth.models import User
from django import forms
from myproject.settings import APP_NAME, DOMAIN, DEFAULT_FROM_EMAIL
from django.core.mail import send_mail
from main.models import Profile, Location
# from django.template.loader import render_to_string

GENDER_CHOICES = (
    ('Male', 'Male'),
    ('Female', 'Female'),
)

CAR_NO_CAR_CHOICES = (
    ('No Car', 'No Car'),
    ('Has Car', 'Has Car'),
)


class UserForm(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput())
    password2 = forms.CharField(widget=forms.PasswordInput())
    # Location attributes
    area = forms.CharField(max_length=256, widget=forms.TextInput(attrs={'readonly': True}))
    gender = forms.ChoiceField(choices=GENDER_CHOICES, label="", initial='', widget=forms.Select(), required=True)
    has_car = forms.ChoiceField(choices=CAR_NO_CAR_CHOICES, required=True)
    longitude = forms.CharField(max_length=256)
    latitude = forms.CharField(max_length=256)
    upper_bound = forms.CharField(max_length=256)
    lower_bound = forms.CharField(max_length=256)

    def __init__(self, *args, **kwargs):

        super(UserForm, self).__init__(*args, **kwargs)

        for key in self.fields:
            self.fields[key].required = True
            self.fields[key].widget.attrs['class'] = 'form-control'

        # help tects
        self.fields['username'].help_text = ''

        # labels
        self.fields['gender'].label = 'Gender'
        self.fields['has_car'].label = 'Car Status'

        self.fields['email'].label = 'Email'
        self.fields['password1'].label = 'Password'
        self.fields['password2'].label = 'Password Confirmation'
        self.fields['area'].label = 'Where Do You Live'
        self.fields['longitude'].label = ''
        self.fields['latitude'].label = ''
        self.fields['upper_bound'].label = ''
        self.fields['lower_bound'].label = ''

        # Hidden Input
        self.fields['longitude'].widget = forms.HiddenInput()
        self.fields['latitude'].widget = forms.HiddenInput()
        self.fields['upper_bound'].widget = forms.HiddenInput()
        self.fields['lower_bound'].widget = forms.HiddenInput()

    class Meta:
        model = User
        fields = ('username', 'email', 'password1',
                  'password2', 'first_name')

    def clean(self):
        if (
                'password1' in self.cleaned_data and
                'password2' in self.cleaned_data):
            if(
                    self.cleaned_data['password1'] !=
                    self.cleaned_data['password2']):
                self.add_error(
                    'password1', "Passwords doesn't match")
        return self.cleaned_data

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.filter(username=username):
            self.add_error('username', 'Username already exists')
        return username

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email):
            self.add_error('email', 'Email already exists')
        return email

    def save(self):
        new_user = User.objects.create_user(
            username=self.cleaned_data['username'],
            password=self.cleaned_data[
                'password1'],
            email=self.cleaned_data['email'],
            first_name=self.cleaned_data['first_name'],
        )
        new_location = Location.objects.create(area=self.cleaned_data['area'], longitude=self.cleaned_data['longitude'], latitude=self.cleaned_data['latitude'], lower_bound=self.cleaned_data['lower_bound'], upper_bound=self.cleaned_data['upper_bound'])
        Profile.objects.create(user=new_user, location=new_location, gender=self.cleaned_data['gender'], car=self.cleaned_data['has_car'])
        self.send_sign_up_email()
        return new_user

    def send_sign_up_email(self):
        subject = "Welcome to " + APP_NAME + "!"
        msg = "You have been successfully added as a member of " + \
            APP_NAME + " Community\n\n"
        # message = render_to_string('main/welcome_email.html', {})
        msg += "To Login please click on the following link:\n"
        msg += "http://" + DOMAIN + "/auth/user/login"
        msg += "\n\n"
        send_mail(
            subject, msg,
            DEFAULT_FROM_EMAIL,
            [self.cleaned_data['email']],
            # html_message=message,
            fail_silently=False)
