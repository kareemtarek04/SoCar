from .forms import UserForm
from django.http import JsonResponse
from django.views.generic import FormView
from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.core.urlresolvers import reverse
from main import helpers
from django.contrib.auth.decorators import login_required
# from django.http import HttpResponseRedirect
# from django.core.urlresolvers import reverse_lazy
# from django.contrib.auth import  update_session_auth_hash
# from django.shortcuts import render
# from django.contrib import messages
# from django.contrib.auth.forms import PasswordChangeForm
# from django.utils.decorators import method_decorator
# from django.views.decorators.csrf import csrf_exempt


class LoginRequiredMixin(object):

    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)


class LoginView(FormView):

    """
    User Login Class.

    """

    def get(self, request, *args, **kwargs):
        username = request.GET.get('username')
        password = request.GET.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return JsonResponse({'success': 1})
            else:
                return JsonResponse({'success': 0,
                                     'error':
                                     "Your account has been disabled"})

        else:
            return JsonResponse({'success': 0,
                                 'error': "Invalid login details supplied."})


class UserRegistrationView(FormView):
    """
    User Sign Up Class.
    """
    form_class = UserForm
    template_name = 'registration/SignUp.html'

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
            # return JsonResponse({'err_list': self.get_form().errors,
            #                      'success': 0})

    def form_valid(self, form):
        helpers.update_hit("sign_up")
        user = form.save()
        self.new_user = user
        user = authenticate(
            username=self.request.POST['username'],
            password=self.request.POST['password1'])
        update_session_auth_hash(self.request, user)
        login(self.request, user)
        return super(UserRegistrationView, self).form_valid(form)

    def get_success_url(self):
        return reverse('profile', kwargs={'pk': self.new_user.id})


# class UserChangePassword(LoginRequiredMixin, FormView):
#     """
#     Class to allow user to change his/her password.
#     """

#     def get(self, request, *args, **kwargs):
#         """
#         Initializes form for user registration
#         """
#         err1 = "Confirmation password didn't match your new password"
#         err2 = "Your old password is not correct"
#         error_list = []
#         missing = False
#         if request.is_ajax():
#             old_pass = request.GET['old_pass']
#             pass1 = request.GET['pass1']
#             pass2 = request.GET['pass2']
#             if not old_pass:
#                 error_list.append('Old Password field is required')
#                 missing = True
#             if not pass1:
#                 error_list.append('New Password field is required')
#                 missing = True
#             if not pass2:
#                 error_list.append('Confirmation Password field is required')
#                 missing = True
#             if missing:
#                 return JsonResponse({'err_list': error_list,
#                                      'success': 0})
#             if request.user.check_password(old_pass):
#                 if pass1 == pass2:
#                     request.user.set_password(pass1)
#                     request.user.save()
#                     user = authenticate(
#                         username=request.user.username,
#                         password=pass1)
#                     login(self.request, user)
#                     return JsonResponse({'success': 1})
#                 else:
# New password and confirmation didnt match
#                     error_list.append(err1)
#             else:
#                 error_list.append(err2)
#             return JsonResponse({'err_list': error_list,
#                                  'success': 0})
#         form = PasswordChangeForm(user=request.user)
#         return render(
# request, 'registration/user-change-password.html', {'form': form})

#     def post(self, request, *args, **kwargs):
#         """
#         Validated registration form and auto-login the new user.
#         Author: Moustafa
#         """
#         form = PasswordChangeForm(user=request.user, data=request.POST)
#         if form.is_valid():
#             form.save()
#             update_session_auth_hash(request, form.user)
#             messages.add_message(
#                 self.request, messages.INFO,
#                 'Your Password Has Been Changed Succesfuly.')
#             return HttpResponseRedirect(reverse('main:project-listing'))

#         return render(
# request, 'registration/user-change-password.html', {'form': form})
