
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .views import UserRegistrationView
# from django.views.generic import TemplateView
# from .views import UserChangePassword, LoginView


urlpatterns = [
    url(r'^user/new/$', UserRegistrationView.as_view(), name='user-new'),
    # url(r'^user/logout/$', auth_views.logout,
    #     {'next_page': '/auth/user/login/'}, name='user-logout'),
    url(r'^user/logout/$', auth_views.logout,
        {'next_page': '/'}, name='user-logout'),
    url(r'^user/login/$', auth_views.login,
        {'template_name': 'registration/login.html'},
        name='user-login'),
    # url(
    #     r'^user/reset/$', auth_views.password_reset,
    #     {'template_name': 'registration/password_reset.html'},
    #     name='user-reset'),
    # url(
    #     r'^resetpassword/passwordsent/$',
    #     auth_views.password_reset_done,
    #     {'template_name': 'registration/reset_done.html'},
    #     name='password_reset_done'),
    # url(
    #     r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
    #     auth_views.password_reset_confirm,
    #     {'template_name': 'registration/confirm_reset.html'},
    #     name='password_reset_confirm'),
    # url(
    #     r'^reset/done/$', auth_views.login,
    #     name='password_reset_complete'),
    # url(r'^user/manage/$', TemplateView.as_view(
    #     template_name='registration/manage-account.html'), name='user-manage'),
    # url(r'^user/change-password/$', UserChangePassword.as_view(),
    #     name='user-change-password'),

]
